//1 create database
CREATE DATABSE training;

//2creating table
CREATE TABLE members( membership_number INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, full_names VARCHAR(30) NOT NULL, gender VARCHAR(30) NOT NULL, date_of_birth DATE, physical_address VARCHAR(10), postal_address VARCHAR(20), contact_number VARCHAR(13), email VARCHAR(255) );

CREATE TABLE movies( movie_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, title VARCHAR(30) NOT NULL, director VARCHAR(30) NOT NULL, year_released VARCHAR(5),category_id INT(6) );


//5 insert query
INSERT INTO members (full_names, gender, date_of_birth,physical_address,postal_address,contact_number,email)
VALUES ('janet jonas', 'female',1980-01-21,'first street plot no.4','private bag','0789283898','janetjonas@yahoo.com');

INSERT INTO members (full_names, gender, date_of_birth,physical_address,postal_address,contact_number,email)
VALUES ('janet smith jonas', 'female',1980-06-23,'melrose 123',NULL,NULL,'jj@fstreet.com');

INSERT INTO members (full_names, gender, date_of_birth,physical_address,postal_address,contact_number,email)
VALUES ('robert phill', 'male',1980-06-23,'3rd street 34',NULL,'1234','rm@tstreet.com');

INSERT INTO members (full_names, gender, date_of_birth,physical_address,postal_address,contact_number,email)
VALUES ('gloria williams', 'female',1980-06-23,'2nd street 23',NULL,NULL,NULL);

INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (NULL, 'pirates of carabian 4', 'Rob marshall', '2011', '1');

INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (NULL, 'forgetting sarah marshal', 'nicholas stoller', '2008', '2');

INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (NULL, 'x-men', null, '2008', null)

INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (NULL, 'code name black', 'edgar jims', '2010', null)


INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (NULL, 'daddy\'s little girls', NULL, '2007', '8')

INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (NULL, 'angels and demons', NULL, '2007', '6');

INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (NULL, 'davinci code', NULL, '2007', '6');

INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (NULL, 'honey mooners', 'john schultz', '2005', '8');

INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES ('16', '67%guilty', NULL, '2012', NULL);


//3 select query
SELECT DISTINCT full_names FROM members;



SELECT * FROM `members` WHERE gender='female'

SELECT gender FROM `members` GROUP by gender

SELECT * FROM `movies` GROUP BY `category_id`,`year_released` HAVING `category_id` = 8


//4 select query
SELECT * FROM `movies` WHERE year_released IN('2007','2010')


SELECT * FROM `movies` WHERE year_released='2007' OR category_id=6

SELECT * FROM `movies` WHERE year_released='2007' AND category_id=6

SELECT * FROM `movies` WHERE NOT year_released='2007'

SELECT * FROM `movies` WHERE year_released NOT IN('2007','2006')

SELECT * FROM `movies` WHERE category_id=6

SELECT * FROM `movies` WHERE category_id <> 6

SELECT * FROM `movies` WHERE category_id > 6

SELECT * FROM `movies` WHERE category_id < 6

SELECT * FROM `movies`

//6 getting records from members
SELECT * FROM `members`

//7 select records based on given column names
SELECT full_names,gender,physical_address,email FROM `members`


//8 select data whose id is 1
SELECT * FROM `members` WHERE membership_number=1

//9 selectdata based on condition
SELECT * FROM `movies` WHERE category_id=2 AND year_released='2008'

//10 selectdata based on condition
SELECT * FROM `movies` WHERE category_id=2 OR category_id=1

//11 selectdata based on condition
SELECT * FROM `members` WHERE membership_number=1 OR membership_number=2 OR membership_number=3

//12 select data whose gender is female
SELECT * FROM `members` WHERE gender='female'

//counting the movies grouping by year released then sorting the movies then print 2nd row
SELECT * ,COUNT(year_released ) 'year count ' from movies GROUP by year_released ORDER BY COUNT(year_released ) LIMIT 1,1